package com.artmal.part2;

import com.artmal.part2.cof.*;

public class Program {
    public static void main(String[] args) {
        DispenseChain fileNameOption = new FileNameFilterDispenser();
        DispenseChain fileExtensionOption = new FileExtensionDispenser();
        DispenseChain fileSizeOption = new FileSizeDispanser();
        DispenseChain fileChangeDateOption = new FileChangeDateDispanser();

        fileChangeDateOption.setNextChain(fileExtensionOption);
        fileExtensionOption.setNextChain(fileSizeOption);
        fileSizeOption.setNextChain(fileChangeDateOption);

        Filter filter = new Filter("E:\\");
        fileNameOption.dispense(filter);
    }
}