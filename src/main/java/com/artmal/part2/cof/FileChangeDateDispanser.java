package com.artmal.part2.cof;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Scanner;

public class FileChangeDateDispanser implements DispenseChain {
    private DispenseChain nextChain;

    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    public void dispense(Filter filter) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Search by file date change? (0/1)");
        int choice = Integer.parseInt(sc.nextLine());

        if(choice == 1) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

            System.out.println("Enter min date");
            DateTime minChangeDate = formatter.parseDateTime(sc.nextLine());
            filter.setMinDateOfEdit(minChangeDate);

            System.out.println("Enter max date");
            DateTime maxChangeDate = formatter.parseDateTime(sc.nextLine());
            filter.setMaxDateOfEdit(maxChangeDate);
        }

        sc.close();
        nextChain.dispense(filter);
    }
}