package com.artmal.part2.cof;

import java.util.Scanner;

public class FileNameFilterDispenser implements DispenseChain {
    private DispenseChain nextChain;

    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    public void dispense(Filter filter) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Search by filename? (0/1)");
        int choice = Integer.parseInt(sc.nextLine());

        if(choice == 1) {
            System.out.println("Enter filename");
            String filename = sc.nextLine();
            filter.setFilename(filename);
        }

        sc.close();
        nextChain.dispense(filter);
    }
}
