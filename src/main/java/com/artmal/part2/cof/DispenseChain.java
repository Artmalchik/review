package com.artmal.part2.cof;

public interface DispenseChain {
    void setNextChain(DispenseChain nextChain);
    void dispense(Filter filter);
}
