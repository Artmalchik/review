package com.artmal.part2.cof;

import org.joda.time.DateTime;

public class Filter {
    private String directory;
    private String filename;
    private String extension;
    private int minKb;
    private int maxKb;
    private DateTime minDateOfEdit;
    private DateTime maxDateOfEdit;

    public Filter() {
    }

    public Filter(String directory) {
        this.directory = directory;
    }

    public String getDirectory() {
        return directory;
    }
    public void setDirectory(String directory) {
        this.directory = directory;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public String getExtension() {
        return extension;
    }
    public void setExtension(String extension) {
        this.extension = extension;
    }
    public int getMinKb() {
        return minKb;
    }
    public void setMinKb(int minKb) {
        this.minKb = minKb;
    }
    public int getMaxKb() {
        return maxKb;
    }
    public void setMaxKb(int maxKb) {
        this.maxKb = maxKb;
    }
    public DateTime getMinDateOfEdit() {
        return minDateOfEdit;
    }
    public void setMinDateOfEdit(DateTime minDateOfEdit) {
        this.minDateOfEdit = minDateOfEdit;
    }
    public DateTime getMaxDateOfEdit() {
        return maxDateOfEdit;
    }
    public void setMaxDateOfEdit(DateTime maxDateOfEdit) {
        this.maxDateOfEdit = maxDateOfEdit;
    }
}
