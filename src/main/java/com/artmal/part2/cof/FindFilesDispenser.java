package com.artmal.part2.cof;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;

/**
 * End of chain.
 */
public class FindFilesDispenser implements DispenseChain {
    public void setNextChain(DispenseChain nextChain) {
    }

    public void dispense(Filter filter) {
        File root = new File(filter.getDirectory());

        Collection files = FileUtils.listFiles(root, null, true);
        if (filter.getFilename() != null) {
            
        }


    }
}
