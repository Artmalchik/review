package com.artmal.part2.cof;

import java.util.Scanner;

public class FileSizeDispanser implements DispenseChain {
    private DispenseChain nextChain;

    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    public void dispense(Filter filter) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Search by file size? (0/1)");
        int choice = Integer.parseInt(sc.nextLine());

        if(choice == 1) {
            System.out.println("Enter min size in kb");
            int minKb = Integer.parseInt(sc.nextLine());
            filter.setMinKb(minKb);

            System.out.println("Enter max size in kb");
            int maxKb = Integer.parseInt(sc.nextLine());
            filter.setMaxKb(maxKb);
        }

        sc.close();
        nextChain.dispense(filter);
    }
}