package com.artmal.part2.cof;

import java.util.Scanner;

public class FileExtensionDispenser implements DispenseChain {
    private DispenseChain nextChain;

    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    public void dispense(Filter filter) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Search by file extension? (0/1)");
        int choice = Integer.parseInt(sc.nextLine());

        if(choice == 1) {
            System.out.println("Enter extension");
            String fileExtension = sc.nextLine();
            filter.setFilename(fileExtension);
        }

        sc.close();
        nextChain.dispense(filter);
    }
}
